<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\BooksController;
use App\Http\Controllers\PatronsController;
use App\Http\Controllers\BorrowedbooksController;
use App\Http\Controllers\ReturnedbooksController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/ 
Route::get('category',[CategoryController::class, 'show']);
Route::post('books',[BooksController::class, 'store']);
Route::get('books',[BooksController::class, 'show']);
Route::post('patrons',[PatronsController::class, 'store']);
Route::get('patorns',[PatronsController::class, 'show']);
Route::post('borrowedbooks',[BorrowedbooksController::class, 'store']);
Route::get('borrowedbooks',[BorrowedbooksController::class, 'show']);
Route::post('borrowedbooks',[BorrowedbooksController::class, 'store']);
Route::get('borrowedbooks',[BorrowedbooksController::class, 'show']);
Route::post('returnedbooks',[ReturnedbooksController::class, 'store']);
Route::get('returnedbooks',[ReturnedbooksController::class, 'show']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); 
