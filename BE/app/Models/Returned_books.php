<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Returned_books extends Model
{
    public function borrowed_books()
    {
        return $this->belongsToMany(Borrowed_books::class, 'borrowed_books');
    }
    public function patrons()
    {
        return $this->belongsToMany(Patrons::class, 'patrons');
    }
    use HasFactory;
}
