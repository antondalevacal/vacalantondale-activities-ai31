<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patrons extends Model
{
    public function borrowed_books()
    {
        return $this->belongsToMany(Borrowed_books::class, 'borrowed_books');
    }
    public function returned_books()
    {
        return $this->belongsToMany(Returned_books::class, 'returned_books');
    }
    use HasFactory;
}
