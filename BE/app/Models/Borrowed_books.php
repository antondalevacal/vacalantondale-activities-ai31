<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Borrowed_books extends Model
{
	public function books()
    {
        return $this->belongsToMany(Books::class, 'books');
    }
    public function patrons()
    {
        return $this->belongsToMany(Patrons::class, 'patrons');
    }
    use HasFactory;
}
