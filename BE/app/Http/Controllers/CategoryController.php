<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\CategoryResouce;

class CategoryController extends Controller
{
    public function show(Category $category): CategoryResouce
    {
    	return new CategoryResource($category);
    }
}
