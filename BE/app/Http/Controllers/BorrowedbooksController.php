<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\BorrowedbooksResouce;

class BorrowedbooksController extends Controller
{
	public function store(Borrowedbooks $borrowedbooks): BorrowedbooksResource
	{
    	return new BooksResource($borrowedbooks);
	}
    
	public function show(Borrowedbooks $borrowedbooks): BorrowedbooksResource 
	{
    	return new BorrowedbooksResource($borrowedbooks);
    }
}
