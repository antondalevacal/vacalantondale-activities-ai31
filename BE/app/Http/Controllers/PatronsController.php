<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\PatronsResouce;

class PatronsController extends Controller
{
	public function store(Patrons $patorns): PatronsResource
	{
    	return new PatronsResource($patrons);
	}
    
	public function show(Patrons $patrons): PatronsResource 
	{
    	return new PatronsResource($patrons);
    }
}
