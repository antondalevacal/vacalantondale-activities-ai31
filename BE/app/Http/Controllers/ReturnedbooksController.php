<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ReturnedbooksResouce;

class ReturnedbooksController extends Controller
{
	public function store(Returnedbooks $returnedbooks): ReturnedbooksResource
	{
    	return new ReturnedbooksResource($returnedbooks);
	}
    
	public function show(Returnedbooks $returnedbooks): ReturnedbooksResource 
	{
    	return new ReturnedbooksResource($returnedbooks);
    }
}
