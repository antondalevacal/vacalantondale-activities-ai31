<?php

namespace App\Http\Controllers;

use App\Books;
use Illuminate\Http\Request;
use App\Http\Resources\BooksResouce;

class BooksController extends Controller
{
   
	public function store(Books $books): BooksResource
	{
    	return new BooksResource($books);
	}
    
	public function show(Books $books): BooksResource 
	{
    	return new BooksResource($books);
    }
}
