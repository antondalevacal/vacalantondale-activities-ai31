var days = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];

var ctx = document.getElementById("myChart");
new Chart(ctx, {
  type: 'bar',
  data: {
    labels: days,
    datasets: [
        { 
          data: [15, 12, 7, 20, 18, 10, 22],
          label: "Books Borowed",
          borderColor: "#3e95cd",
          backgroundColor: "#3743F7",
          fill: false
        },
        { 
          data: [10, 5, 8, 14, 16, 21, 18],
          label: "Books Returned",
          borderColor: "#3e95cd",
          backgroundColor: "#CC123B",
          fill: false
        },
    ]
  }
});

var pie = document.getElementById('myChart2').getContext('2d');
new Chart(pie, {
    type: 'pie',
    data : {
        labels: ["Horror","Romance","Fantasy","Action","Drama"],
        datasets: [
            {
                label: "Book Genres",
                backgroundColor: ["#f1c40f","#e67e22","#16a085","#2980b9"],
                data: [10, 20, 55, 30, 10]
            }
        ]
    }, 
    options: {
        cutoutPercentage: 30,
        animation: {
            animationScale: true
        }
    }
});