import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '../components/Dashboard.vue'
import Patron from '../components/Patron.vue'
import Book from '../components/Book.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'dashboard',
    component: Dashboard
  },
  {
	path: '/patrons',
	name: 'patrons',
	component: Patron
  },
  {
	path: '/books',
	name: 'books',
	component: Book
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router