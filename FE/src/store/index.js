import Vue from "vue";
import Vuex from "vuex";
import boos from "./modules/books.js";
import borrowed from "./modules/borrowed.js";
import categories from "./modules/categories.js";
import patrons from "./modules/patons.js";
import returned from "./modules/returned.js";

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		books,
		borrowed,
		categories,
		patrons,
		returned
	},
})