Ha sakob im public waray assets?

Ha sakob im public waray assets?
Waray men
Sige i kopya ko nala
Pta kayanomay toastr folder mo wa ko man hito
Yaa ine HAHAHA
Adi gin butangan ko nalan fodlers adi na assets haak src
Pta kayanomay toastr folder mo wa ko man hito
Igbutang nala gihap men
Pag install nala niyan
npm install toastr --save
Niyan nala
Ano mayda na config folder?
Ano mayda na config folder?
Meron na meb
Tapos na
Wait la men
Sigemeb
May config na men? Folder?
Pag add hin file men

axiosConfig.js
Tapos na men
Butangan ko na ghap hin sulod
Na copy mo tanan men?
Anay send ko ha codes
Na copy mo tanan men?
Oo men
import axios from "@/assets/js/config/axiosConfig.js";
import toastr from "toastr";

export default {
    namespaced: true,

    state: {
        books: [],
    },
    getters: {
        getBooks: (state) => state.books,
    },
    mutations: {
        setBooks: (state, books) => (state.books = books),
        setDeleteBook: (state, id) =>
            (state.books = state.books.filter((book) => book.id !== id)),
        setNewBook: (state, book) => state.books.unshift({ ...book }),
        setUpdateBook: (state, updatedBook) => {
            const index = state.books.findIndex((book) => book.id === updatedBook.id);
            if (index !== -1) {
                state.books.splice(index, 1, { ...updatedBook });
            }
        },
    },
    actions: {
        async addNewBook({ commit }, book) {
            await axios
                .post("/books", book)
                .then((res) => {
                    commit("setNewBook", res.data.book);
                    toastr.success(res.data.message, "Success");
                })
                .catch((err) => {
                    if (err.response.status == 422) {
                        for (var i in err.response.data.errors) {
                            toastr.error(err.response.data.errors[i][0], "Failed");
                        }
                    }
                });
        },
        async deleteBook({ commit }, id) {
            await axios.delete(`/booksdelete/${id}`)
                .then(res => {
                    commit("setDeleteBook", id);
                    toastr.success(res.data.message, "Success");
                })
                .catch(err => {
                    console.error(err.response.data);
                })

        },
        async fetchBooks({ commit }) {
            await axios
                .get("/books")
                .then((res) => {
                    commit("setBooks", res.data);
                })
                .catch((err) => {
                    console.error(err);
                });

        },
        async updateBook({ commit }, book) {

            var id = book.id
            var body = {
                name: book.name,
                author: book.author,
                copies: book.copies,
                category_id: book.category_id
            }

            await axios
                .put(`/booksupdate/${id}`, body)
                .then((res) => {
                    toastr.success(res.data.message, "Success");
                    commit("setUpdateBook", res.data.book);
                })
                .catch((err) => {
                    if (err.response.status == 422) {
                        for (var i in err.response.data.errors) {
                            toastr.error(err.response.data.errors[i][0], "Failed");
                        }
                    }
                });
        },
    },
};
paste mo yan men sa book.js
Sige men
sa modules
igedit nala kun anot ieedit
Tapos anot sunod men
import axios from "@/assets/js/config/axiosConfig";
import toastr from "toastr";

export default {
    namespaced: true,

    state: {
        borrowedbooks: [],
    },
    getters: {
        getBorrowedBooks: (state) => state.borrowed,
    },
    mutations: {
        setBorrowedBook: (state, borrowed) => state.borrowed.push({ ...borrowed }),
        setBorrowedBooks: (state, borrowedBooks) =>
            (state.borrowed = borrowedBooks),
    },
    actions: {
        async borrowedBook({ commit }, borrow) {
            await axios
                .post("/borrowedbooks", borrow)
                .then((res) => {
                    toastr.success(res.data.message, "Success");
                    commit("setBorrowedBook", res.data.borrowed);
                })
                .catch((err) => {
                    if (err.response.status == 422) {
                        for (var i in err.response.data.errors) {
                            toastr.error(err.response.data.errors[i][0], "Failed");
                        }
                    }
                });
        },
        async fetchBorrowedBooks({ commit }) {
            await axios
                .get("/borrowedbooks")
                .then((res) => {
                    commit("setBorrowedBooks", res.data);
                })
                .catch((err) => {
                    console.error(err);
                });
        },
    },
};
adi kanan borrowed
borrowed.js
Tapos na
import axios from "@/assets/js/config/axiosConfig";

export default {
    namespaced: true,

    state: {
        categories: [],
    },
    getters: {
        getCategories: (state) => state.categories,
    },
    mutations: {
        setCategories: (state, categories) => (state.categories = categories),
    },
    actions: {
        async fetchCategories({ commit }) {
            await axios
                .get("/categories")
                .then((res) => {
                    commit("setCategories", res.data);
                })
                .catch((res) => {
                    console.log(res);
                });
        },
    },
};
categories
categories.js
okay na men
import axios from "@/assets/js/config/axiosConfig";
import toastr from 'toastr';

export default {
    namespaced: true,

    state: {
        patrons: []
    },
    getters: {
        getPatrons: (state) => state.patrons
    },
    mutations: {
        setPatrons: (state, patrons) => state.patrons = patrons,
        setDeletePatron: (state, id) =>
            (state.patrons = state.patrons.filter((patron) => patron.id !== id)),
        setNewPatron: (state, patron) => state.patrons.unshift({ ...patron }),
        setUpdatePatron: (state, updatedPatron) => {
            const index = state.patrons.findIndex((patron) => patron.id === updatedPatron.id);
            if (index !== -1) {
                state.patrons.splice(index, 1, { ...updatedPatron });
            }
        },
    },
    actions: {
        async createPatron({ commit }, patron) {
            await axios
                .post("/patrons", patron)
                .then((res) => {
                    commit("setNewPatron", res.data.patron);
                    toastr.success(res.data.message, "Success");
                })
                .catch((err) => {
                    if (err.response.status == 422) {
                        for (var i in err.response.data.errors) {
                            toastr.error(err.response.data.errors[i][0], "Failed");
                        }
                    }
                });
        },
        async deletePatron({ commit }, id) {
            await axios.delete(`/patrons/${id}`)
                .then(res => {
                    commit("setDeletePatron", id);
                    toastr.success(res.data.message, "Success");
                })
                .catch(err => {
                    console.error(err);
                })

        },
        async fetchPatrons({ commit }) {
            await axios
                .get("/patrons")
                .then((res) => {
                    commit("setPatrons", res.data);
                })
                .catch((err) => {
                    console.error(err);
                });

        },
        async updatePatron({ commit }, patron) {

            var id = patron.id
            var body = {
                last_name: patron.last_name,
                first_name: patron.first_name,
                middle_name: patron.middle_name,
                email: patron.email
            }

            await axios
                .put(`/patrons/${id}`, body)
                .then((res) => {
                    toastr.success(res.data.message, "Success");
                    commit("setUpdatePatron", res.data.patron);
                })
                .catch((err) => {
                    if (err.response.status == 422) {
                        for (var i in err.response.data.errors) {
                            toastr.error(err.response.data.errors[i][0], "Failed");
                        }
                    }
                });
        },
    },
};