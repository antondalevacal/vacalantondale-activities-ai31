document.getElementById("dashboardclick").onclick = function() { 
    document.getElementById("bookwindow").style.visibility = "hidden";
    document.getElementById("patronwindow").style.visibility = "hidden";
    document.getElementById("dashboardwindow").style.visibility = "visible"; 
    } 
document.getElementById("patronclick").onclick = function() { 
    document.getElementById("bookwindow").style.visibility = "hidden";
    document.getElementById("dashboardwindow").style.visibility = "hidden"; 
    document.getElementById("patronwindow").style.visibility = "visible"; 
    } 
document.getElementById("bookclick").onclick = function() { 
    document.getElementById("patronwindow").style.visibility = "hidden"; 
    document.getElementById("dashboardwindow").style.visibility = "hidden";
    document.getElementById("bookwindow").style.visibility = "visible";
    } 
 

var days = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];

var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: days,
    datasets: [
		{ 
		  data: [15, 12, 7, 20, 18, 10, 22],
		  label: "Books Borowed",
		  borderColor: "#3e95cd",
		  backgroundColor: "#3743F7",
		  fill: false
		},
		{ 
		  data: [10, 5, 8, 14, 16, 21, 18],
		  label: "Books Returned",
		  borderColor: "#3e95cd",
		  backgroundColor: "#CC123B",
		  fill: false
		},
    ]
  }
});

var ctx = document.getElementById("myChart2");
var myChart2 = new Chart(ctx, {
    type: 'doughnut',
    data : {
    	labels: ["Horror","Romance","Fantasy","Action","Drama"],
    	datasets: [
    		{
    			label: "Book Genres",
    			backgroundColor: ["#f1c40f","#e67e22","#16a085","#2980b9"],
    			data: [10, 20, 55, 30, 10]
    		}
    	]
    }, 
    options: {
    	cutoutPercentage: 30,
    	animation: {
    		animationScale: true
    	}
    }
});